import { Component } from '@angular/core';
import { ServiceService } from './services/service.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  offers: Array<any> = []
  selectedOffer: any = ''
  constructor(private service: ServiceService){

  }

  ngOnInit(): void {
    this.service.getData().subscribe((data: any) => {
      this.offers = data
    })   
  }

  changeOffer(offer){
    this.selectedOffer = offer[0];
    console.log(offer)
  }
}
